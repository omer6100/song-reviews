import { Component, OnInit } from '@angular/core';
import { artist } from 'src/assets/DM/artist';
import { reviews } from 'src/assets/DM/reviews';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
  
  public artists:artist[]=[];
  public reviews: reviews[]=[] ;
  
  constructor() {
    for(let i=0;i<5;i++){
      this.reviews.push(new reviews());
    }
    for(let i=0;i<10;i++){
      this.artists.push(new artist());
    }
   }

  ngOnInit(): void {
  }

}
