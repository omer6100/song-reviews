import { song } from "./song";

export class artist{
    image:string;
    TopSongs:song[];
    ArtistName:string;
    followers:number;
    
    constructor(){
        
        this.TopSongs=[];
        this.ArtistName="";
        this.image="https://i.scdn.co/image/ab67616d0000b273f4b8a43495c6172243cf16f4";
        this.followers=0;
        
    }
}